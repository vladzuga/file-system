
public class Rmdir implements Command {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "rmdir";
	}
	
	public void action (Current c, String path, String args) {
		Cd tmp = new Cd();
		Directory init = c.currentDirectory;
		String[] tokens = path.split("/");
		path = "";
		for (int i = 0; i < tokens.length - 1; i ++)
			path += tokens[i] + "/";
		
		tmp.action(c, path, this, args);
		if ( c.currentDirectory != null ) {
			int dirIndex = Directory.getDirectoryIndex(c.currentDirectory, tokens[tokens.length - 1]);
			int fileIndex = File.getFileIndex(c.currentDirectory, tokens[tokens.length - 1]);
			
			if (tokens[tokens.length - 1].equals(c.currentDirectory.name) ) {
				ErrorsPrint.err = Errors.ERROR13;
				ErrorsPrint.printError(this, args);
			}
			if (!c.currentDirectory.name.equals("/")) {
				if (tokens[tokens.length - 1].equals(c.currentDirectory.parent.name)) {
					ErrorsPrint.err = Errors.ERROR13;
					ErrorsPrint.printError(this, args);
				}
			}
			if (dirIndex == -1) {
				if (fileIndex != -1) {
					ErrorsPrint.err = Errors.ERROR3;
					ErrorsPrint.printError(this, args);
				}
				else {
					ErrorsPrint.err = Errors.ERROR2;
					ErrorsPrint.printError(this, args);
				}
			}
			else if ( c.currentDirectory.owner.equals(c.currentUser.list.get(0))) {
				if( (c.currentDirectory.perm.getUsersPermissions() & 2 ) != 2) {
					ErrorsPrint.err = Errors.ERROR5;
					ErrorsPrint.printError(this, args);
				}
				else {
					c.currentDirectory.getSubordinates().remove(dirIndex);
				}	
			}
			else if ( (c.currentDirectory.perm.getOthersPermissions() & 2 ) != 2) {
				ErrorsPrint.err = Errors.ERROR5;
				ErrorsPrint.printError(this, args);
			}
			else {
				if ( !c.currentDirectory.getSubordinates().isEmpty() ) {
					ErrorsPrint.err = Errors.ERROR14;
					ErrorsPrint.printError(this, args);
				}
				c.currentDirectory.getSubordinates().remove(dirIndex);
			}
		}
		else
			c.currentDirectory = init;
	}

}
