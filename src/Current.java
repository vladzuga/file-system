
public class Current {
	public Users currentUser;
	public Directory currentDirectory;
	
	public Current (Users u, Directory d) {
		currentUser = u;
		currentDirectory = d;
	}
}
