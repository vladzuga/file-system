
public class Touch implements Command {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "touch";
	}
	
	public void action (Current c, String path, String args) {
		Cd tmp = new Cd();
		Directory init = c.currentDirectory;
		String[] tokens = path.split("/");
		path = "";
		for (int i = 0; i < tokens.length - 1; i ++)
			path += tokens[i] + "/";
		
		if ( path != "" )
			tmp.action(c, path, this, args);
		if ( c.currentDirectory != null ) {
			if ( c.currentDirectory.getSubordinates().contains(tokens[tokens.length-1]) ) {
				if ( c.currentDirectory.getSubordinates().
						get(c.currentDirectory.getSubordinates().indexOf(tokens[tokens.length-1])) 
						instanceof File) {
					ErrorsPrint.err = Errors.ERROR7;
					ErrorsPrint.printError(this, args);
				}
				else {
					ErrorsPrint.err = Errors.ERROR1;
					ErrorsPrint.printError(this, args);
				}
			}
			if ( c.currentDirectory.owner.equals(c.currentUser.list.get(0))) {
				if( (c.currentDirectory.perm.getUsersPermissions() & 2 ) != 2 && !c.currentUser.list.get(0).equals("root")) {
					ErrorsPrint.err = Errors.ERROR5;
					ErrorsPrint.printError(this, args);
				}
				else
					c.currentDirectory.addNew(new File(false, tokens[tokens.length-1], c.currentDirectory, c.currentUser.list.get(0)));
			}
			else if ( (c.currentDirectory.perm.getOthersPermissions() & 2 ) != 2 && !c.currentUser.list.get(0).equals("root")) {
				ErrorsPrint.err = Errors.ERROR5;
				ErrorsPrint.printError(this, args);
			}
			else
				c.currentDirectory.addNew(new File(false, tokens[tokens.length-1], c.currentDirectory, c.currentUser.list.get(0)));
		}
		else
			c.currentDirectory = init;
	}

}
