
public class ChUser implements Command {

	@Override
	public String getName() {
		return "chuser";
	}
	
	public void action ( Current c, String changeUser, Users allUsers, String args ) {
		if( !allUsers.list.contains(changeUser) ) {
			ErrorsPrint.err = Errors.ERROR8;
			ErrorsPrint.printError(this, args);
		}
		else {
			c.currentUser.changeCurrentUser(changeUser);
			Directory.changeCurrentDirectory(c, changeUser);
		}
	}

}
