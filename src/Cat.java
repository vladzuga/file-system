
public class Cat implements Command {

	@Override
	public String getName() {
		return "cat";
	}
	
	public void action (Current c, String path, String args) {
		Directory tmp = c.currentDirectory;
		
		String[] tokens = path.split("/");
		path = "";
		for (int i = 0; i < tokens.length - 1; i ++)
			path += tokens[i] + "/";
		
		Cd cd = new Cd();
		cd.action(c, path, this, args);
		
		int dirIndex = Directory.getDirectoryIndex(c.currentDirectory, tokens[tokens.length - 1]);
		if ( dirIndex != -1 ) {
			ErrorsPrint.err = Errors.ERROR1;
			ErrorsPrint.printError(this, args);
		}
		else {
			int fileIndex = File.getFileIndex(c.currentDirectory, tokens[tokens.length - 1]);
			if ( fileIndex == -1 ) {
				ErrorsPrint.err = Errors.ERROR11;
				ErrorsPrint.printError(this, args);
			}
			else if ( c.currentDirectory.owner.equals(c.currentUser.list.get(0))) {
				if( (c.currentDirectory.perm.getUsersPermissions() & 1 ) != 1 && !c.currentUser.list.get(0).equals("root")) {
					ErrorsPrint.err = Errors.ERROR6;
					ErrorsPrint.printError(this, args);
				}
				else {
					File f = (File)c.currentDirectory.getSubordinates().get(fileIndex);
					if ( c.currentDirectory.owner.equals(c.currentUser.list.get(0))) {
						if ( (f.perm.getUsersPermissions() & 4) != 4 && !c.currentUser.list.get(0).equals("root")) {
							ErrorsPrint.err = Errors.ERROR4;
							ErrorsPrint.printError(this, args);
						}
						else
							System.out.println(f.getContent());
					}
					else if ( (f.perm.getOthersPermissions() & 4) != 4 && !c.currentUser.list.get(0).equals("root")) {
						ErrorsPrint.err = Errors.ERROR4;
						ErrorsPrint.printError(this, args);
					}
					else
						System.out.println(f.getContent());
				}
			}
			else if ( (c.currentDirectory.perm.getOthersPermissions() & 1 ) != 1 && !c.currentUser.list.get(0).equals("root")) {
				ErrorsPrint.err = Errors.ERROR6;
				ErrorsPrint.printError(this, args);
			}
			else {
				File f = (File)c.currentDirectory.getSubordinates().get(fileIndex);
				System.out.println(f.getContent());
			}
		}
		
		c.currentDirectory = tmp;
	}

}
