import java.util.ArrayList;

public class Directory extends Entity {
	private ArrayList<Entity> subordinates;

	public Directory(boolean isRoot, String name, Directory parent, String owner) {
		subordinates = new ArrayList<Entity>();
		this.name = name;
		perm = new Permissions(isRoot);
		if (!isRoot)
			this.parent = parent;
		else
			this.parent = null;
		this.owner = owner;
	}
	
	@Override
	public boolean descendants() {
		return true;
	}
	
	public ArrayList<Entity> getSubordinates () {
		return this.subordinates;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void addNew(Entity e) {
		subordinates.add(e);
	}
	
	public static void changeCurrentDirectory(Current curr, String changeU) {
		curr.currentDirectory = Directory.getRoot(curr.currentDirectory);
		if ( !changeU.equals("root") ) {
			for (Entity e : curr.currentDirectory.subordinates) {
				if ( e.name.equals(changeU )) {
					curr.currentDirectory = (Directory) e;
					break;
				}
			}
		}
	}
	
	public static Directory searchForUserDirectory (String dName, Directory currentD) {
		while ((!currentD.getName().equals(dName)) && currentD.parent != null) {
			currentD = currentD.parent;
		}
		return currentD;
	}
	
	public static Directory getRoot(Directory currentD) {
		//get to '\'
		while (currentD.parent != null) {
			currentD = currentD.parent;
		}
		return currentD;
	}
	
	public void updateOwner(String deleteU, String newU) {
		if (!this.subordinates.isEmpty()) {
			for (Entity e : subordinates) {
				if ( e.owner.equals(deleteU)) {
					e.owner = newU;
				}
				if ( e.descendants() )
					((Directory)e).updateOwner(deleteU, newU);
			}
		}
	}
	
	public static int getDirectoryIndex (Directory currentD, String dName) {
		int k = 0;
		for (Entity e : currentD.subordinates) {
			if ( e.descendants() ) {
				Directory tmp = (Directory) e;
				if ( tmp.name.equals(dName) )
					return k;
			}
			k ++;
		}
		return -1;
	}
	
	public Directory changeDirectory (String path, Command c, String currentU, String args) {
		if (path.equals("."))
			return this;
		if (path.equals("..")) {
			if ( !this.name.equals("/") )
				return this.parent;
			else
				return this;
		}
		int index = Directory.getDirectoryIndex(this, path);
		if ( index == -1 ) {
			ErrorsPrint.err = Errors.ERROR2;
			ErrorsPrint.printError(c, args);
		}
		else if ( !(this.subordinates.get(index) instanceof Directory ) ) {
			ErrorsPrint.err = Errors.ERROR3;
			ErrorsPrint.printError(c, args);
		}
		else if ( this.owner == currentU ) {
			if ( !((this.perm.getUsersPermissions() & 1) == 1) && !currentU.equals("root")) {
				ErrorsPrint.err = Errors.ERROR6;
				ErrorsPrint.printError(c, args);
			}
			return (Directory)this.subordinates.get(index);
		}
		else if ( !((this.perm.getOthersPermissions() & 1) == 1) && !currentU.equals("root")) {
			ErrorsPrint.err = Errors.ERROR6;
			ErrorsPrint.printError(c, args);
		}
		else {
			return (Directory)this.subordinates.get(index);
		}
		return null;
	}

}
