
public class Rm implements Command {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "rm";
	}
	
	public void action (Current c, String path, String args) {
		Cd tmp = new Cd();
		Directory init = c.currentDirectory;
		String[] tokens = path.split("/");
		if ( tokens.length == 0 ) {
			String[] s = {"/"};
			tokens = s.clone(); 
		}
		path = "";
		for (int i = 0; i < tokens.length - 1; i ++)
			path += tokens[i] + "/";
		
		tmp.action(c, path, this, args);
		if (c.currentDirectory != null) {
			if ( !args.contains("-r")) {
				int index = File.getFileIndex(c.currentDirectory , tokens[tokens.length - 1]);
				if ( c.currentDirectory.getSubordinates().contains(tokens[tokens.length - 1]) ) {
					if ( c.currentDirectory.getSubordinates().
							get(c.currentDirectory.getSubordinates().indexOf(tokens[tokens.length-1])) 
							instanceof Directory) {
						ErrorsPrint.err = Errors.ERROR1;
						ErrorsPrint.printError(this, args);
					}
				}
				else if ( index == -1 ) {
					ErrorsPrint.err = Errors.ERROR11;
					ErrorsPrint.printError(this, args);
				}
				else if ( (c.currentDirectory.perm.getOthersPermissions() & 1 ) != 1) {
					ErrorsPrint.err = Errors.ERROR5;
					ErrorsPrint.printError(this, args);
				}
				else
					c.currentDirectory.getSubordinates().remove(index);
			}
			else {
				int fileIndex = File.getFileIndex(c.currentDirectory, tokens[tokens.length - 1]);
				int dirIndex = Directory.getDirectoryIndex(c.currentDirectory, tokens[tokens.length - 1]);
				if ( fileIndex == -1 && dirIndex == -1 && !tokens[tokens.length - 1].equals("/") ) {
					ErrorsPrint.err = Errors.ERROR12;
					ErrorsPrint.printError(this, args);
				}
				else if (tokens[tokens.length - 1].equals(c.currentDirectory.name) ) {
					ErrorsPrint.err = Errors.ERROR13;
					ErrorsPrint.printError(this, args);
				}
				else if (!c.currentDirectory.name.equals("/")) {
					if (tokens[tokens.length - 1].equals(c.currentDirectory.parent.name)) {
						ErrorsPrint.err = Errors.ERROR13;
						ErrorsPrint.printError(this, args);
					}
				}
				else if ( c.currentDirectory.owner.equals(c.currentUser.list.get(0))) {
					if( (c.currentDirectory.perm.getUsersPermissions() & 2 ) != 2) {
						ErrorsPrint.err = Errors.ERROR5;
						ErrorsPrint.printError(this, args);
					}
					else
						if ( fileIndex != -1 )
							c.currentDirectory.getSubordinates().remove(c.currentDirectory.getSubordinates().get(fileIndex));
						else
							c.currentDirectory.getSubordinates().remove(c.currentDirectory.getSubordinates().get(dirIndex));
				}
				else if ( (c.currentDirectory.perm.getOthersPermissions() & 2 ) != 2) {
					ErrorsPrint.err = Errors.ERROR5;
					ErrorsPrint.printError(this, args);
				}
				else {
					if ( fileIndex != -1 )
						c.currentDirectory.getSubordinates().remove(c.currentDirectory.getSubordinates().get(fileIndex));
					else
						c.currentDirectory.getSubordinates().remove(c.currentDirectory.getSubordinates().get(dirIndex));
				}
			}
		}
		else
			c.currentDirectory = init;
	}

}
