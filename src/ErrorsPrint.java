
public class ErrorsPrint {
	public static Errors err;
	
	public static void printError (Command c, String args) {
		switch (err) {
		case ERROR1:
			System.out.println("-1: " + c.getName() + args + ": Is a directory");
			break;
		case ERROR2:
			System.out.println("-2: " + c.getName() + args + ": No such directory");
			break;
		case ERROR3:
			System.out.println("-3: " + c.getName() + args + ": Not a directory");
			break;
		case ERROR4:
			System.out.println("-4: " + c.getName() + args + ": No rights to read");
			break;
		case ERROR5:
			System.out.println("-5: " + c.getName() + args + ": No rights to write");
			break;
		case ERROR6:
			System.out.println("-6: " + c.getName() + args + ": No rights to execute");
			break;
		case ERROR7:
			System.out.println("-7: " + c.getName() + args + ": File already exists");
			break;
		case ERROR8:
			System.out.println("-8: " + c.getName() + args + ": User does not exist");
			break;
		case ERROR9:
			System.out.println("-9: " + c.getName() + args + ": User already exists");
			break;
		case ERROR10:
			System.out.println("-10: " + c.getName() + args +  ": No rights to change user status");
			break;
		case ERROR11:
			System.out.println("-11: " + c.getName() + args + ": No such file");
			break;
		case ERROR12:
			System.out.println("-12: " + c.getName() + args + ": No such file or directory");
			break;
		case ERROR13:
			System.out.println("-13: " + c.getName() + args + ": Cannot delete parent or current directory");
			break;
		case ERROR14:
			System.out.println("-14: " + c.getName() + args + ": Non empty directory");
			break;
		}
	}
}
