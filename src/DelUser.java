
public class DelUser implements Command {

	@Override
	public String getName() {
		return "deluser";
	}

	public void action( Current c, String deleteUser, Users allUsers, String args ) {
		if( !allUsers.list.contains(deleteUser) ) {
			ErrorsPrint.err = Errors.ERROR8;
			ErrorsPrint.printError(this, args);
		}
		else if ( !c.currentUser.list.get(0).equals("root") ) {
			ErrorsPrint.err = Errors.ERROR10;
			ErrorsPrint.printError(this, args);
		}
		else {
			allUsers.list.remove(deleteUser);
			Directory tmp = Directory.getRoot(c.currentDirectory);
			String newUser;
			if ( allUsers.list.get(1).equals("root"))
				newUser = allUsers.list.get(0);
			else
				newUser = allUsers.list.get(1);
			tmp.updateOwner(deleteUser, newUser);
		}
	}
}
