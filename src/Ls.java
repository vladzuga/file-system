
public class Ls implements Command {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "ls";
	}
	
	public void print (Entity e) {
		System.out.print(e.name + " ");
		if ( e instanceof Directory )
			System.out.print("d");
		else
			System.out.print("f");
		if ( ((e.perm.getUsersPermissions() & 4) == 4) )
			System.out.print("r");
		else
			System.out.print("-");
		if ( ((e.perm.getUsersPermissions() & 2) == 2) )
			System.out.print("w");
		else
			System.out.print("-");
		if ( ((e.perm.getUsersPermissions() & 1) == 1) )
			System.out.print("x");
		else
			System.out.print("-");
		if ( ((e.perm.getOthersPermissions() & 4) == 4) )
			System.out.print("r");
		else
			System.out.print("-");
		if ( ((e.perm.getOthersPermissions() & 2) == 2) )
			System.out.print("w");
		else
			System.out.print("-");
		if ( ((e.perm.getOthersPermissions() & 1) == 1) )
			System.out.print("x");
		else
			System.out.print("-");
		System.out.println(" " + e.owner);
	}
	
	public void action(Current c, String path, String args) {
		Directory tmp = c.currentDirectory;
		Cd cd = new Cd();
		cd.action(c, path, this, args);
		
		for (Entity e : c.currentDirectory.getSubordinates()) {
			print(e);
		}
		
		c.currentDirectory = tmp;
	}

}
