
public class Cd implements Command {

	@Override
	public String getName() {
		return "cd";
	}
	
	public void action (Current c, String path, Command cmd, String args) {
		String[] tokens = path.split("/");
		
		int k = 0;
		if ( path.startsWith("/") )
			c.currentDirectory = Directory.getRoot(c.currentDirectory);
		while (k < tokens.length) {
			if ( !tokens[k].equals("") )
				c.currentDirectory = c.currentDirectory.changeDirectory(tokens[k], cmd, c.currentUser.list.get(0), args);
			if (c.currentDirectory == null)
				return;
			k ++;
		}
		if ( c.currentDirectory.owner == c.currentUser.list.get(0)) {
			if ( !((c.currentDirectory.perm.getUsersPermissions() & 1) == 1) && !c.currentUser.list.get(0).equals("root") && (cmd instanceof Cd 
					|| cmd instanceof Mkdir) ) {
				ErrorsPrint.err = Errors.ERROR6;
				ErrorsPrint.printError(cmd, args);
				c.currentDirectory = null;
			}
		}
		else if ( !((c.currentDirectory.perm.getOthersPermissions() & 1) == 1) && !c.currentUser.list.get(0).equals("root") && (cmd instanceof Cd
				|| cmd instanceof Mkdir) ) {
			ErrorsPrint.err = Errors.ERROR6;
			ErrorsPrint.printError(cmd, args);
			c.currentDirectory = null;
		}
	}

}
