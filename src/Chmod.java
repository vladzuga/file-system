
public class Chmod implements Command {

	@Override
	public String getName() {
		return "chmod";
	}
	
	public void action (Current c, String path, int x, String args) {
		
		Directory tmp = c.currentDirectory;
		
		String[] tokens = path.split("/");
		path = "";
		for (int i = 0; i < tokens.length - 1; i ++)
			path += tokens[i] + "/";
		
		Cd cd = new Cd();
		cd.action(c, path, this, args);
		
			
		int fileIndex = File.getFileIndex(c.currentDirectory, tokens[tokens.length - 1]);
		int dirIndex = Directory.getDirectoryIndex(c.currentDirectory, tokens[tokens.length - 1]);
		
		if ( fileIndex == -1 && dirIndex == -1 ) {
			ErrorsPrint.err = Errors.ERROR12;
			ErrorsPrint.printError(this, args);
		}
		else {
			if ( fileIndex != -1 ) {
				if ( c.currentDirectory.getSubordinates().get(fileIndex).owner.equals(c.currentUser.list.get(0))) {
					if( (c.currentDirectory.getSubordinates().get(fileIndex).perm.getUsersPermissions() & 2 ) != 2 
							&& !c.currentUser.list.get(0).equals("root")) {
						ErrorsPrint.err = Errors.ERROR5;
						ErrorsPrint.printError(this, args);
					}
					else
						c.currentDirectory.getSubordinates().get(fileIndex).perm.setPermissions(x);
				}
				else if ( (c.currentDirectory.getSubordinates().get(fileIndex).perm.getOthersPermissions() & 2 ) != 2 
						&& !c.currentUser.list.get(0).equals("root")) {
					ErrorsPrint.err = Errors.ERROR5;
					ErrorsPrint.printError(this, args);
				}
				else
					c.currentDirectory.getSubordinates().get(fileIndex).perm.setPermissions(x);
			}
			else {
				if ( c.currentDirectory.owner.equals(c.currentUser.list.get(0))) {
					if( (c.currentDirectory.getSubordinates().get(dirIndex).perm.getUsersPermissions() & 2 ) != 2 
							&& !c.currentUser.list.get(0).equals("root")) {
						ErrorsPrint.err = Errors.ERROR5;
						ErrorsPrint.printError(this, args);
					}
					else
						c.currentDirectory.getSubordinates().get(dirIndex).perm.setPermissions(x);
				}
				else if ( (c.currentDirectory.getSubordinates().get(dirIndex).perm.getOthersPermissions() & 2 ) != 2 
						&& !c.currentUser.list.get(0).equals("root")) {
					ErrorsPrint.err = Errors.ERROR5;
					ErrorsPrint.printError(this, args);
				}
				else
					c.currentDirectory.getSubordinates().get(dirIndex).perm.setPermissions(x);
			}
		}
		
		c.currentDirectory = tmp;

	}
}
