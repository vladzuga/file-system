
public class File extends Entity {
	private String content;
	
	public File(boolean isRoot, String name, Directory parent, String owner) {
		this.name = name;
		perm = new Permissions(isRoot);
		if (!isRoot)
			this.parent = parent;
		else
			this.parent = null;
		this.owner = owner;
	}

	@Override
	public boolean descendants() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public String getContent () {
		return content;
	}
	
	public void writeContent(String s) {
		content = s;
	}
	
	public static int getFileIndex (Directory currentD, String dName) {
		int k = 0;
		for (Entity e : currentD.getSubordinates()) {
			if ( !e.descendants() ) {
				File tmp = (File) e;
				if ( tmp.name.equals(dName) )
					return k;
			}
			k ++;
		}
		return -1;
	}

}
