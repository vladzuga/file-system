
public class AddUser implements Command {

	@Override
	public String getName() {
		return "adduser";
	}
	
	public void action (Current c, String newUser, Users allUsers, String args ) {
		if( allUsers.list.contains(newUser) ) {
			ErrorsPrint.err = Errors.ERROR9;
			ErrorsPrint.printError(this, args);
		}
		else if ( !c.currentUser.list.get(0).equals("root") ) {
			ErrorsPrint.err = Errors.ERROR10;
			ErrorsPrint.printError(this, args);
		}
		else {
			Directory tmp = Directory.getRoot(c.currentDirectory);
			tmp.addNew(new Directory(false, newUser, c.currentDirectory, newUser));
			allUsers.list.add(newUser);
		}
	}

}
