
public abstract class Entity {
	protected String name;
	protected Permissions perm;
	protected String owner;
	protected Directory parent;
	
	public Entity() {}
	
	abstract public boolean descendants();
}
