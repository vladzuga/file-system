
public class Permissions {
	/*
	public boolean ownerR;
	public boolean ownerW;
	public boolean ownerX;
	public boolean othersR;
	public boolean othersW;
	public boolean othersX;
	*/
	
	private int user;
	private int others;
	
	public Permissions (boolean isRoot) {
		user = 7;
		if (isRoot == false)
			others = 0;
		else
			others = 5;
	}
	
	public void setPermissions(int x) {
		int y = x % 10;
		x /= 10;
		user = x;
		others = y;
	}
	
	public void updatePermissions(String newU) {
		user = 7;
		if ( newU.equals("root") )
			others = 5;
		else
			others = 0;
	}
	
	public int getUsersPermissions() {
		return user;
	}
	
	public int getOthersPermissions() {
		return others;
	}
}
