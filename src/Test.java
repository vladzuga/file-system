
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Test {
	
	public static void print(Entity d, String offset) {
		System.out.print(offset + d.name + " ");
		if (d.descendants())
			System.out.print("d");
		else
			System.out.print("f");
		if ( (d.perm.getUsersPermissions() & 4) == 4 )
			System.out.print("r");
		else
			System.out.print("-");
		if ( (d.perm.getUsersPermissions() & 2) == 2 )
			System.out.print("w");
		else
			System.out.print("-");
		if ( (d.perm.getUsersPermissions() & 1) == 1 )
			System.out.print("x");
		else
			System.out.print("-");
		if ( (d.perm.getOthersPermissions() & 4) == 4 )
			System.out.print("r");
		else
			System.out.print("-");
		if ( (d.perm.getOthersPermissions() & 2) == 2 )
			System.out.print("w");
		else
			System.out.print("-");
		if ( (d.perm.getOthersPermissions() & 1) == 1 )
			System.out.print("x");
		else
			System.out.print("-");
		System.out.println(" " + d.owner);
		
		offset += "\t";
		if (d.descendants()) {
			Directory t = (Directory) d;
			if ( t.getSubordinates().size() != 0 ) {
				for (Entity e : t.getSubordinates() ) {
					Test.print(e, offset);
				}
			}
		}
	}
	
	public static void main (String[] Args) {
		try {

			List<String> lines = Files.readAllLines(Paths.get("in7.txt"), Charset.defaultCharset());
			Current curr = new Current(new Users(), new Directory(true, "/", null, "root"));
			Users allUsers = new Users();
			
			for (String line : lines) {
				String[] args = line.split("\\s+");
				
				if ( args[0].equals("adduser")) {
					AddUser a = new AddUser();
					a.action(curr, args[1], allUsers, " " + args[1]);
				}
				
				if ( args[0].equals("chuser")) {
					ChUser a = new ChUser();
					a.action(curr, args[1], allUsers, " " + args[1]);
				}
				
				if ( args[0].equals("cd")) {
					Directory init = curr.currentDirectory;
					Cd a = new Cd();
					a.action(curr, args[1], a, " " + args[1]);
					if ( curr.currentDirectory == null )
						curr.currentDirectory = init;
				}
				
				if ( args[0].equals("chmod")) {
					Chmod a = new Chmod();
					a.action(curr, args[2], Integer.parseInt(args[1]), " " + args[1] + " " + args[2]);
				}
				
				if ( args[0].equals("deluser")) {
					DelUser a = new DelUser();
					a.action(curr, args[1], allUsers, " " + args[1]);
				}
				
				if ( args[0].equals("ls")) {
					Ls a = new Ls();
					a.action(curr, args[1], " " + args[1]);
				}
				
				if ( args[0].equals("mkdir")) {
					Directory tmp = curr.currentDirectory;
					if ( !args[1].equals("/")) {
						Mkdir a = new Mkdir();
						a.action(curr, args[1], " " + args[1]);
					}
					curr.currentDirectory = tmp;
				}
				
				if ( args[0].equals("touch")) {
					Directory tmp = curr.currentDirectory;
					Touch a = new Touch();
					a.action(curr, args[1]," " + args[1]);
					curr.currentDirectory = tmp;
				}
				
				if ( args[0].equals("rm")) {
					Directory tmp = curr.currentDirectory;
					Rm a = new Rm();
					if ( args.length == 2 ) {
						a.action(curr, args[1], " " + args[1]);
					}
					else {
						a.action(curr, args[2], " " + args[1] + " " + args[2]);
					}
					curr.currentDirectory = tmp;
				}
				
				if ( args[0].equals("rmdir")) {
					Directory tmp = curr.currentDirectory;
					Rmdir a = new Rmdir();
					a.action(curr, args[1], " " + args[1]);
					curr.currentDirectory = tmp;
				}
				
				if ( args[0].equals("writetofile")) {
					Directory tmp = curr.currentDirectory;
					WriteToFile a = new WriteToFile();
					String s = "";
					for (int i = 2; i < args.length - 1; i ++) {
						s = s.concat(args[i] + " ");
					}
					s = s.concat(args[args.length - 1]);
					a.action(curr, args[1], s);
					curr.currentDirectory = tmp;
				}
				
				if ( args[0].equals("cat")) {
					Directory tmp = curr.currentDirectory;
					Cat a = new Cat();
					a.action(curr, args[1], " " + args[1]);
					curr.currentDirectory = tmp;
				}
				
			}

			curr.currentDirectory = Directory.getRoot(curr.currentDirectory);
			Test.print(curr.currentDirectory, "");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
